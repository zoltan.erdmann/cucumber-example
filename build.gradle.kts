plugins {
	id("java")
	id("org.springframework.boot") version "2.6.4"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
}

val cucumberRuntime: Configuration by configurations.creating {
	extendsFrom(configurations["testImplementation"])
}

group = "org.example.cucumber"
version = "1.0-SNAPSHOT"

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")

	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
	testImplementation(platform("io.cucumber:cucumber-bom:7.2.3"))
	testImplementation("io.cucumber:cucumber-spring")
	testImplementation("io.cucumber:cucumber-java")
	testImplementation("io.cucumber:cucumber-junit-platform-engine")
}

configurations.all {
	if (name.contains("cucumberRuntime")) {
		attributes.attribute(Usage.USAGE_ATTRIBUTE, objects.named(Usage::class.java, Usage.JAVA_RUNTIME))
	}
}

tasks.getByName<Test>("test") {
	useJUnitPlatform()
	systemProperty("cucumber.junit-platform.naming-strategy", "long")
}

task("cucumber") {
	dependsOn("assemble")
	dependsOn("compileTestJava")
	doLast {
		javaexec {
			mainClass.set("io.cucumber.core.cli.Main")
			classpath = cucumberRuntime + sourceSets.main.get().output + sourceSets.test.get().output
			args = listOf(
				"--plugin", "pretty",
				"--glue", "${project.group}",
				"src/test/resources/features"
			)
		}
	}
}
