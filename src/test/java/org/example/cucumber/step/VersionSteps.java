// SPDX-License-Identifier: MIT

package org.example.cucumber.step;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VersionSteps {

	@Autowired
	private TestRestTemplate restTemplate;

	private String returnedVersion;

	@Given("a started application")
	public void a_started_application() {
		// Intentional SKIP.
	}

	@When("^the (.*) endpoint is called$")
	public void endpointCalled(String endpoint) {
		var response = restTemplate.getForEntity(endpoint, String.class);
		returnedVersion = response.getBody();
	}

	@Then("the version is returned")
	public void theCorrectVersionReturned() {
		assertEquals("1.0.0", returnedVersion);
	}
}
