# SPDX-License-Identifier: MIT
Feature: Version functions
  Version related tests

  Scenario: Getting the version
    Given a started application
    When the /version endpoint is called
    Then the version is returned
